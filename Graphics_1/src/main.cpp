/*  by Alun Evans 2016 LaSalle (aevanss@salleurl.edu) */

#include <string>
#define TINYOBJLOADER_IMPLEMENTATION
#include "tiny_obj_loader.h"
#include "imageloader.h"

std::string basepath = "assets/";
std::string inputfile_bunny = basepath + "sphere.obj";
std::string earth = basepath + "sphere.obj";

// Shapes for moon
std::vector< tinyobj::shape_t > shapes;
std::vector< tinyobj::material_t > materials;

//Shapes for earth
std::vector< tinyobj::shape_t > shapes_earth;
std::vector< tinyobj::material_t > materials_earth;


std::string err;
std::string err_earth;

bool ret = tinyobj::LoadObj(shapes, materials, err, inputfile_bunny.c_str(), basepath.c_str());
bool ret_earth = tinyobj::LoadObj(shapes_earth, materials_earth, err_earth, earth.c_str(), basepath.c_str());


//include some standard libraries
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>

//include OpenGL libraries
#include <GL/glew.h>
#include <GLFW/glfw3.h>

//include some custom code files
#include "glfunctions.h" //include all OpenGL stuff
#include "Shader.h" // class to compile shaders

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>


using namespace std;
using namespace glm;

//global variables to help us do things
int g_ViewportWidth = 512; int g_ViewportHeight = 512; // Default window size, in pixels
double mouse_x, mouse_y; //variables storing mouse position
const vec3 g_backgroundColor(0.2f, 0.2f, 0.2f); // background colour - a GLM 3-component vector

GLuint g_simpleShader = 0; //shader identifier
GLuint g_Vao = 0; //vao
GLuint g_NumTriangles = 0; //  Numbre of triangles we are painting.
GLuint texture_id;

// Define variables 
float FoV = 45.0f;
float speed = 3.0f;
float mouseSpeed = 0.005f;
glm::vec3 eye(1.2f, 1.2f, 1.2f);
glm::vec3 center(0.0f, 0.0f, 0.0f);
glm::vec3 up(0.0f, 1.0f, 0.0f);
glm::vec3 front = center - eye;
glm::vec3 side = up - front;


// Define matrices that we are going to use
glm::mat4 Model = mat4(1.0); //this is equal to identity matrix
glm::mat4 Projection = perspective(
	FoV, // Field of view
	1.0f,  // Aspect ratio
	0.1f,  // near plane (distance from camera)
	100.0f  // Far plane (distance from camera)
);
glm::mat4 View = glm::lookAt(
	eye, //eye - Camera is at (4,3,3), in World Space
	center, // center - and looks at the origin
	up //up - Head is up(set to 0, -1, 0 to look upside - down)
);

//For the camera
glm::vec3 position = vec3(1.2f, 1.2f, 1.2f);
glm::vec3 viewDirection = vec3(0.0f, 0.0f, 0.0f);
const glm::vec3 UP = vec3(0.0f, 1.0f, 0.0f);
glm::vec2 oldMousePosition;
static const float MOV_SPEED = 0.1f;
glm::vec3 strafeDir;

// Booleans to let us know which key is pressed
bool A_pressed = false;
bool W_pressed = false;
bool S_pressed = false;
bool D_pressed = false;
bool Q_pressed = false;
bool Z_pressed = false;
bool up_pressed = false;
bool down_pressed = false;
bool left_pressed = false;
bool right_pressed = false;

// ------------------------------------------------------------------------------------------
// This function manually creates a square geometry (defined in the array vertices[])
// ------------------------------------------------------------------------------------------
void load()
{
	//**********************
	// CODE TO LOAD EVERYTHING INTO MEMORY
	//**********************

	//load the shader
	Shader simpleShader("src/shader.vert", "src/shader.frag");
	g_simpleShader = simpleShader.program;

	// Create the VAO where we store all geometry (stored in g_Vao)
	g_Vao = gl_createAndBindVAO();

	gl_createAndBindAttribute(&shapes[0].mesh.positions[0], shapes[0].mesh.positions.size() * sizeof(float), g_simpleShader, "a_vertex", 3);
	gl_createIndexBuffer(&shapes[0].mesh.indices[0], shapes[0].mesh.indices.size() * sizeof(unsigned int));

	GLfloat* uvs = &(shapes[0].mesh.texcoords[0]);
	GLuint uvs_size = shapes[0].mesh.texcoords.size() * sizeof(GLfloat);

	gl_createAndBindAttribute(uvs, uvs_size, g_simpleShader, "a_uv", 2);

	//unbind everything
	gl_unbindVAO();

	//store number of triangles (use in draw())
	g_NumTriangles = shapes[0].mesh.indices.size() / 3;

	// Load the image
	Image* image = loadBMP("assets/earthmap1k.bmp");
	glGenTextures(1, &texture_id);
	glBindTexture(GL_TEXTURE_2D, texture_id);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image->width, image->height, 0, GL_RGB, GL_UNSIGNED_BYTE, image->pixels);

}

// ------------------------------------------------------------------------------------------
// This function actually draws to screen and called non-stop, in a loop
// ------------------------------------------------------------------------------------------

void draw()
{
	float now = glfwGetTime();
	float lastTime = 0;
	float delta = now - lastTime;
	lastTime = now;

	//clear the screen
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// activate shader
	glUseProgram(g_simpleShader);
	GLuint colorLoc = glGetUniformLocation(g_simpleShader, "u_color");
	glUniform3f(colorLoc, 0.7, 1.0, 0.0);

	// Using the View matrix
	if (W_pressed) {
		eye.z -= 0.001;
		View = glm::lookAt(eye, center, up);
	}
	if (S_pressed) {
		eye.z += 0.001;
		View = glm::lookAt(eye, center, up);
	}
	if (D_pressed) {
		eye.x -= 0.001;
		View = glm::lookAt(eye, center, up);
	}
	if (A_pressed) {
		eye.x += 0.001;
		View = glm::lookAt(eye, center, up);
	}

	// Up and down
	if (up_pressed) {
		center.z -= 0.001;
		View = glm::lookAt(eye, center, up);
	}
	if (down_pressed) {
		center.z += 0.001;
		View = glm::lookAt(eye, center, up);
	}
	if (left_pressed) {
		center.x -= 0.001;
		View = glm::lookAt(eye, center, up);
	}
	if (right_pressed) {
		center.x += 0.001;
		View = glm::lookAt(eye, center, up);
	}


	GLuint model_loc = glGetUniformLocation(g_simpleShader, "Model");
	glUniformMatrix4fv(model_loc, 1, GL_FALSE, glm::value_ptr(Model));

	GLuint projection_loc = glGetUniformLocation(g_simpleShader, "Projection");
	glUniformMatrix4fv(projection_loc, 1, GL_FALSE, glm::value_ptr(Projection));

	GLint view_loc = glGetUniformLocation(g_simpleShader, "View");
	glUniformMatrix4fv(view_loc, 1, GL_FALSE, glm::value_ptr(View));

	glBindVertexArray(g_Vao);

	//bind the geometry
	gl_bindVAO(g_Vao);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	GLuint u_texture = glGetUniformLocation(g_simpleShader, "u_texture");
	glUniform1i(u_texture, 0);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture_id);

	// Draw to screen
	glDrawElements(GL_TRIANGLES, 3 * g_NumTriangles, GL_UNSIGNED_INT, 0);
}

//glm::mat4 getWorldToViewMatrix() {
//	return glm::lookAt(position, position + viewDirection, UP);
//}


// ------------------------------------------------------------------------------------------
// This function is called every time you press a screen
// ------------------------------------------------------------------------------------------
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods) {
	//quit
	float now = glfwGetTime();
	float lastTime = 0;
	float delta = now - lastTime;
	lastTime = now;

	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, 1);
	//reload
	if (key == GLFW_KEY_R && action == GLFW_PRESS)
		load();

	// Move the objects to the sides
	if (key == GLFW_KEY_D && action == GLFW_PRESS) {
		D_pressed = true;
	}
	else {
		D_pressed = false;
	}
	if (key == GLFW_KEY_A && action == GLFW_PRESS) {
		A_pressed = true;
	}
	else {
		A_pressed = false;
	}
	/*if (key == GLFW_KEY_S && action == GLFW_PRESS) {
	now += delta;
	auto rotateMat = rotate(Model, lastTime * 45.0f,glm::vec3(0.000003f,0.000006f, 1.0f));
	}*/

	// Move the objects forward or backward
	if (key == GLFW_KEY_S && action == GLFW_PRESS) { S_pressed = true; }
	else { S_pressed = false; }

	if (key == GLFW_KEY_W && action == GLFW_PRESS) { W_pressed = true; }
	else { W_pressed = false; }

	if (key == GLFW_KEY_UP && action == GLFW_PRESS) { up_pressed = true; }
	else { up_pressed = false; }

	if (key == GLFW_KEY_DOWN && action == GLFW_PRESS) { down_pressed = true; }
	else { down_pressed = false; }

	if (key == GLFW_KEY_LEFT && action == GLFW_PRESS) { left_pressed = true; }
	else { left_pressed = false; }

	if (key == GLFW_KEY_RIGHT && action == GLFW_PRESS) { right_pressed = true; }
	else { right_pressed = false; }

}

// ------------------------------------------------------------------------------------------
// This function is called every time you click the mouse
// ------------------------------------------------------------------------------------------
void mouse_button_callback(GLFWwindow* window, int button, int action, int mods) {
	float now = glfwGetTime();
	float lastTime = 0;
	float delta = now - lastTime;
	lastTime = now;

	double xpos, ypos;
	glfwGetCursorPos(window, &xpos, &ypos);

	// Reset mouse position for next frame
	//glfwSetCursorPos(window, 1024 / 2, 768 / 2);
/*
	horizontalAngle += mouseSpeed * delta * float(1024 / 2 - xpos);
	verticalAngle += mouseSpeed * delta * float(768 / 2 - ypos);*/
	/*
		glm::vec3 front(
			cos(verticalAngle) * sin(horizontalAngle),
			sin(verticalAngle),
			cos(verticalAngle) * cos(horizontalAngle)
		);

		glm::vec3 side(
			sin(horizontalAngle - 3.14f / 2.0f),
			0,
			cos(horizontalAngle - 3.14f / 2.0f)
		);*/

		//mouseUpdate(&xpos, &ypos);

	if (button == GLFW_MOUSE_BUTTON_LEFT) {
		//cout << "Left mouse down at" << mouse_x << ", " << mouse_y << endl;
		now += delta;
		auto rotateMat = rotate(Model, lastTime * 45.0f, glm::vec3(0.000003f, 0.000006f, 1.0f));
		//Model = glm::translate(Model, glm::vec3(0.004f, -0.0002f, 0.0f));
	}

}

// funcion que haria mover de un lado a otro la camara hacia el bunny

//void mouseUpdate(double posx, double posy) {
//	glm::vec2 mouseDelta = newMousePosition - oldMousePosition;
//	if (glm::length(mouseDelta) > 50.0f) {
//		oldMousePosition = newMousePosition;
//	}
//	const float ROTATIONAL_SPEED = 0.5f;
//	strafeDir = glm::cross(viewDirection, UP);
//	glm::mat4 rotator = glm::rotate(-mouseDelta.x * ROTATIONAL_SPEED, UP) * glm::rotate(-mouseDelta.y * ROTATIONAL_SPEED, strafeDir);
//	viewDirection = glm::mat3(rotator) * viewDirection;
//}

int main(void)
{
	//setup window and boring stuff, defined in glfunctions.cpp
	GLFWwindow* window;
	if (!glfwInit())return -1;
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	window = glfwCreateWindow(g_ViewportWidth, g_ViewportHeight, "Sphere", NULL, NULL);
	if (!window) { glfwTerminate();	return -1; }
	glfwMakeContextCurrent(window);
	glewExperimental = GL_TRUE;
	glewInit();

	//input callbacks
	glfwSetKeyCallback(window, key_callback);
	glfwSetMouseButtonCallback(window, mouse_button_callback);
	glfwSetInputMode(window, GLFW_STICKY_KEYS, 1);

	//load all the resources
	load();

	//test it loaded correctly 
	if (!err.empty()) { // `err` may contain warning message. 
		std::cerr << err << std::endl;
	}

	//print out number of meshes described in file 
	std::cout << "# of shapes : " << shapes.size() << std::endl;

	// Loop until the user closes the window
	while (!glfwWindowShouldClose(window))
	{
		draw();

		// Swap front and back buffers
		glfwSwapBuffers(window);

		// Poll for and process events
		glfwPollEvents();

		//mouse position must be tracked constantly (callbacks do not give accurate delta)
		glfwGetCursorPos(window, &mouse_x, &mouse_y);
	}
	//terminate glfw and exit
	glfwTerminate();
	return 0;
}